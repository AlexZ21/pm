/*
    Users
*/

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT, 
  `login` varchar(255) DEFAULT NULL,     
  `password` varchar(255) DEFAULT NULL,       
   PRIMARY KEY (`id`),
   UNIQUE KEY `idx_login` (`password`)    
);

/*

   Test user 

    INSERT INTO `users`(`login`, `password`)
    VALUES("user", SHA1("user"));
*/

/*
    Secret string
*/

CREATE TABLE IF NOT EXISTS `loginhash` (
  `ip` varchar(15) NOT NULL,                                       
  `hash` varchar(40) NOT NULL,                                      
  `deadline` datetime NOT NULL                                      
) ENGINE=MEMORY;

/*
    Colors
*/

CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT, 
  `name` varchar(255) NOT NULL,     
   PRIMARY KEY (`id`)  
);

/*
    Painters
*/

CREATE TABLE IF NOT EXISTS `painters` (
  `id` int(11) NOT NULL AUTO_INCREMENT, 
  `name` varchar(255) NOT NULL,     
   PRIMARY KEY (`id`)  
);

/*
    Details
*/

CREATE TABLE IF NOT EXISTS `details` (
  `id` int(11) NOT NULL AUTO_INCREMENT, 
  `name` varchar(255) NOT NULL,
  `prepare_price` int(11) NOT NULL,
  `paint_price` int(11) NOT NULL,     
   PRIMARY KEY (`id`)  
);

/*
    Orders
*/

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*
    Orders items
*/

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `detail_id` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `painter_id` int(11) NOT NULL,
  `painter_price` int(11) NOT NULL DEFAULT '0',
  `prepare_price` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;