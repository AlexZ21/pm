<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

class CONF {

    //Database
    const PDO_HOST = 'localhost';
    const PDO_DB = 'pm1';
    const PDO_USER = 'root';
    const PDO_PASS = '007007';
    const PDO_DSN = 'mysql:host=localhost;dbname=pm1';
    const PDO_ATTR_ERRMODE = 0;
    //Path
    const SITE_DIR = '/var/www/pm/';
    const APP_DIR = '/var/www/pm/application/';
    //Templater path
    const VIEW_DIR = '/var/www/pm/application/View/';
    const CACHE_VIEW_DIR = '/var/www/pm/application/Cache/View/';
    //Fenom
    const FENOM_DIR = '/var/www/pm/application/Lib/FenomTemplate';

    //Routes
    static public $ROUTES = [
        [
            '/',
            'GET',
            '\Controllers\Main::drawMain'
        ],
        [
            '/login',
            'POST',
            '\Controllers\User::login'
        ],
        [
            '/login',
            'GET',
            '\Controllers\User::drawLogin'
        ],
        [
            '/logout',
            'ALL',
            '\Controllers\User::logout'
        ],
        [
            '/list/(:any)',
            'GET',
            '\Controllers\Editor1::drawListPage'
        ],
        [
            '/edit/(:any)',
            'GET',
            '\Controllers\Editor1::drawEditPage'
        ],
        [
            '/edit/(:any)',
            'POST',
            '\Controllers\Editor1::save'
        ],
        [
            '/edit/(:any)',
            'DELETE',
            '\Controllers\Editor1::delete'
        ],
        [
            '/create/(:any)',
            'GET',
            '\Controllers\Editor1::drawCreatePage'
        ],
        [
            '/list/(:any)',
            'DELETE',
            '\Controllers\Editor1::deleteObjects'
        ],
        //Order routes
        [
            '/list/orders',
            'DELETE',
            '\Controllers\OrderEditor::deleteObjects'
        ],
        [
            '/create/orders',
            'GET',
            '\Controllers\OrderEditor::drawCreatePage'
        ],
        [
            '/list/orders',
            'GET',
            '\Controllers\OrderEditor::drawListPage'
        ],
        [
            '/edit/orders',
            'GET',
            '\Controllers\OrderEditor::drawEditPage'
        ],
        [
            '/edit/orders',
            'POST',
            '\Controllers\OrderEditor::save'
        ],
        [
            '/edit/orders',
            'DELETE',
            '\Controllers\OrderEditor::delete'
        ]
    ];

}
