<!DOCTYPE html>
<html>
    <head>
        <title>PM</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/public/css/main.css">
        <link rel="stylesheet" type="text/css" href="/public/css/jquery.datetimepicker.css">
    </head>
    <body>

        <div class="wrapper">

            <header>
                <div class="nav-bar">

                    <div class="brand">
                        <a href="/"><h2>{$lang_site_name}</h2></a>
                    </div>

                    <div class="nav-menu">
                        <ul>
                            <li><a href="/list/orders">Заказы</a></li>
                            <li><a href="/list/details">Детали</a></li>
                            <li><a href="/list/colors">Цвета</a></li>
                            <li><a href="/list/painters">Маляры</a></li>
                            <li><a class="btn-nav-menu" href="/create/orders">Создать заказ</a></li>
                        </ul>
                    </div>

                    <div class="user-logout">
                        <a href="/logout">Выход</a>
                    </div>

                </div>
            </header>

            <div class="content-wrapper">
                {$content}
            </div>

            <footer>

            </footer>

        </div>

    </body>
</html>