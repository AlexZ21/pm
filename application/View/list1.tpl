<div class="content-editor">
    <div class="content-sidebar">

        <div class="sidebar-menu">
            <div class="sidebar-menu-group">
                <div class="header">Действия</div>
                <ul class="sidebar-menu-actions">
                    <li class="sidebar-menu-action">
                        <a href="{$createLink}" id="list1-create">
                            <span class="glyphicon glyphicon-file"></span><span>Создать</span>
                        </a>
                    </li> 
                    <li class="sidebar-menu-action">
                        <a href="{$removeLink}" id="list1-delete">
                            <span class="glyphicon glyphicon-trash"></span><span>Удалить</span>
                        </a>
                    </li>             
                </ul>
            </div>
        </div>

    </div>

    <div class="content-body-wrapper">

        <div class="content-body">

            <h1 class="header">{$pagetitle}</h1>

            <div class="tbl list1">

                <div class="tbl-row tbl-header">
                    <div class="tbl-col tbl-col-small">

                    </div>
                    {foreach $tableHeader as $value}
                    <div class="tbl-col">
                        {$value}
                    </div>
                    {/foreach}
                </div>

                {foreach $data as $row}
                <a class="tbl-row" href="{$row['editUrl']}" data-id="{$row['id']}">
                    <div class="tbl-col tbl-col-small">
                        <input type="checkbox">
                    </div>
                    {foreach $row['object'] as $value}
                    <div class="tbl-col">
                        {$value}
                    </div>
                    {/foreach}
                </a>
                {/foreach}

            </div>

        </div>

    </div>
</div>
<script src="/public/js/jquery-2.1.4.min.js"></script>
<script src="/public/js/jquery.serializejson.min.js"></script>
<script src="/public/js/jquery.popupoverlay.js"></script>
<script src="/public/js/main.js"></script>
<script src="/public/js/list1.js"></script>