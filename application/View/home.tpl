<!DOCTYPE html>
<html>
    <head>
        <title>PM</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="/public/css/main.css">
    </head>
    <body>

        <div class="wrapper">

            <header>
                <div class="nav-bar">

                    <div class="brand">
                        <a href="/"><h2>PaintManager</h2></a>
                    </div>

                    <div class="nav-menu">
                        <ul>
                            <li><a>Заказы</a></li>
                            <li><a>Детали</a></li>
                            <li><a>Цвета</a></li>
                            <li><a href="/painters">Маляры</a></li>
                            <li><a class="btn-nav-menu">Создать заказ</a></li>
                        </ul>
                    </div>

                    <div class="user-logout">
                        <a href="/logout">Выход</a>
                    </div>

                </div>
            </header>

            <div class="content-wrapper">
                <div class="content-editor">
                    <div class="content-sidebar">

                        <div class="sidebar-menu">
                            <div class="sidebar-menu-group">
                                <div class="header">Действия</div>
                                <ul class="sidebar-menu-actions">
                                    <li class="sidebar-menu-action">Сохранить</li> 
                                    <li class="sidebar-menu-action">Удалить</li> 
                                    <li class="sidebar-menu-action">Отмена</li>            
                                </ul>
                            </div>
                            <ul class="sidebar-menu-actions">
                                <li class="sidebar-menu-action">Сохранить</li> 
                                <li class="sidebar-menu-action">Удалить</li> 
                                <li class="sidebar-menu-action">Отмена</li>            
                            </ul>
                        </div>

                    </div>

                    <div class="content-body-wrapper">

                        <div class="content-body">

                            <h1 class="header">Детали</h1>

                            <div class="tbl">
                                <div class="tbl-row tbl-header">
                                    <div class="tbl-col">
                                        sdfsdf
                                    </div>
                                    <div class="tbl-col">
                                        sdfsdf
                                    </div>
                                    <div class="tbl-col">
                                        sdfsdf
                                    </div>                                    
                                </div>
                                <div class="tbl-row">
                                    <div class="tbl-col">
                                        sdfsdf
                                    </div>
                                    <div class="tbl-col">
                                        sdfsdf
                                    </div>
                                    <div class="tbl-col">
                                        sdfsdf
                                    </div> 
                                </div>
                                <div class="tbl-row">
                                    <div class="tbl-col">
                                        sdfsdf
                                    </div>
                                    <div class="tbl-col">
                                        sdfsdf
                                    </div>
                                    <div class="tbl-col">
                                        sdfsdf
                                    </div> 
                                </div>                                
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <footer>

            </footer>

        </div>

        <!--        <script src="/public/js/sha1.js"></script>
                <script src="/public/js/auth.js"></script>-->

    </body>
</html>