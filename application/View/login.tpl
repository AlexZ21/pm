<!DOCTYPE html>
<html>
    <head>
        <title>PM</title>
        <meta charset="utf-8">
    </head>
    <body>

        <div class="wrapper">

            <div class="auth-container">

                <form action="/login" method="POST"
                      enctype="multipart/form-data"
                      onsubmit="return onSubmit();">


                    <div class="form-field">
                        <input type="text" name="login" maxlength="255" placeholder="Логин">
                    </div>

                    <div class="form-field">
                        <input type="text" name="password" maxlength="255" placeholder="Пароль">
                    </div>

                    <div class="form-field">
                        <input type="submit" value="Вход" class="btn btn-submit">
                    </div>                    

                    <input type="hidden" name="sec" value="{$securityHash}">
                    <input type="hidden" name="npassword" value="">
                    
                </form>

            </div>

        </div>

        <script src="/public/js/sha1.js"></script>
        <script src="/public/js/auth.js"></script>

    </body>
</html>