<select name="{$name}">
    <option></option>
  {foreach $options as $option}
    <option value="{$option['id']}" {$option['selected']} 
            {foreach $option['attrFields'] as $field} {$field}="{$option[$field]}" {/foreach}>
            {$option['title']}
</option>
  {/foreach}
</select>