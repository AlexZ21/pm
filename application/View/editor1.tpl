<div class="content-editor">
    <div class="content-sidebar">

        <div class="sidebar-menu sidebar-menu-editor1">
            <div class="sidebar-menu-group">
                <div class="header">Действия</div>
                <ul class="sidebar-menu-actions">
                    <li class="sidebar-menu-action">
                        <a href="#" id="editor1-save">
                            <span class="glyphicon glyphicon-floppy-disk"></span><span>Сохранить</span>
                        </a>
                    </li> 
                    <li class="sidebar-menu-action">
                        <a href="#" id="editor1-delete">
                            <span class="glyphicon glyphicon-trash"></span><span>Удалить</span>
                        </a>
                    </li> 
                    <li class="sidebar-menu-action">
                        <a href="{$listLink}" id="editor1-cancel">
                            <span class="glyphicon glyphicon-remove"></span><span>Отмена</span>
                        </a>
                    </li>            
                </ul>
            </div>
        </div>

    </div>

    <div class="content-body-wrapper">

        <div class="content-body">

            <h1 class="header">{$pagetitle}</h1>

            <div class="container">
                <form id="editor1-form" method="POST" action="{$action}" onsubmit="return false;">
                    <input type="hidden" value="{$id}" name="id">
                    {foreach $attrs as $attr}
                    <div class="form-field">
                        <label for="{$attr['name']}">{$attr['title']}</label>{$attr['attr']}
                    </div>
                    {/foreach}
                </form>                
            </div>

        </div>

    </div>
</div>
<script src="/public/js/jquery-2.1.4.min.js"></script>
<script src="/public/js/jquery.serializejson.min.js"></script>
<script src="/public/js/jquery.popupoverlay.js"></script>
<script src="/public/js/main.js"></script>
<script src="/public/js/editor1.js"></script>