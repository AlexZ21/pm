<div class="content-editor">
    <div class="content-sidebar">

        <div class="sidebar-menu sidebar-menu-editor1">
            <div class="sidebar-menu-group">
                <div class="header">Действия</div>
                <ul class="sidebar-menu-actions">
                    <li class="sidebar-menu-action">
                        <a href="#" id="action-print">
                            <span class="glyphicon glyphicon-print"></span><span>Печать</span>
                        </a>
                    </li>
                    <li class="sidebar-menu-action">
                        <a href="#" id="action-save">
                            <span class="glyphicon glyphicon-floppy-disk"></span><span>Сохранить</span>
                        </a>
                    </li> 
                    <li class="sidebar-menu-action">
                        <a href="#" id="action-delete">
                            <span class="glyphicon glyphicon-trash"></span><span>Удалить</span>
                        </a>
                    </li> 
                    <li class="sidebar-menu-action">
                        <a href="{$listLink}" id="action-cancel">
                            <span class="glyphicon glyphicon-remove"></span><span>Отмена</span>
                        </a>
                    </li>            
                </ul>
            </div>
            <div class="sidebar-menu-group">
                <div class="header">Действия с деталями</div>
                <ul class="sidebar-menu-actions">
                    <li class="sidebar-menu-action">
                        <a href="#" id="action-add-detail">
                            <span class="glyphicon glyphicon-plus"></span><span>Добавить деталь</span>
                        </a>
                    </li>          
                </ul>
            </div>            
        </div>

    </div>

    <div class="content-body-wrapper">

        <div class="content-body">

            <h1 class="header">{$pagetitle}</h1>

            <div class="container">
                <form id="editor1-form" method="POST" action="{$action}" onsubmit="return false;">
                    <input type="hidden" value="{$id}" name="id">
                    {foreach $attrs as $attr}
                    <div class="form-field">
                        <label for="{$attr['name']}">{$attr['title']}</label>{$attr['attr']}
                    </div>
                    {/foreach}
                </form>                        
                <div class="form-field order-items-field">

                    <div class="tbl orderlist">

                        <div class="tbl-row tbl-header">
                            <div class="tbl-col">
                                Деталь
                            </div>
                            <div class="tbl-col">
                                Кол-во
                            </div>
                            <div class="tbl-col">
                                Маляр
                            </div>
                            <div class="tbl-col">
                                Оплата маляра
                            </div>
                            <div class="tbl-col">
                                Оплата подготовки
                            </div>
                            <div class="tbl-col tbl-col-icon">

                            </div>
                        </div>


                        {foreach $orderItems as $colorId => $orderItemsData}

                        <div class="color-group" data-color-id="{$colorId}">

                            <div class="tbl-row">
                                <div class="tbl-col">
                                    Цвет: <span class="color-id">{$orderItemsData['orderItemColor']}</span>
                                </div>
                            </div>

                            {foreach $orderItemsData['orderItems'] as $orderItem}

                            <div class="tbl-row tbl-row-data" data-id="{$orderItem['id']}">
                                {foreach $orderItem['attrs'] as $attr}
                                <div class="tbl-col">
                                    {$attr}
                                </div>
                                {/foreach}
                                <div class="tbl-col tbl-col-icon">
                                    <div class="glyphicon glyphicon-trash remove-row-action"></div>
                                </div>
                            </div>

                            {/foreach}

                        </div>

                        {/foreach}

                    </div>



                </div>

            </div>

        </div>

    </div>
</div>

<div id="create-order-item-popup" class="popup">
    <div class="popup-header">
        Редактировать деталь
    </div>
    <div class="popup-content">
        <form id="new-order-item-fields" onsubmit="return false;">
            <div class="form-field">
                {foreach $attrItems as $attr}
                <div class="form-field">
                    <label for="{$attr['name']}">{$attr['title']}</label>{$attr['attr']}
                </div>
                {/foreach}
            </div>
        </form>
    </div>
    <div class="popup-footer">
        <button class="btn btn-default btn-close">Отмена</button>
        <button class="btn btn-default btn-add" onclick="addOrderItem();">Сохранить</button>
    </div>
</div>

<div id="append-color" class="color-group" style="display: none;">
    <div class="tbl-row">
        <div class="tbl-col">
            Цвет: <span class="color-id"></span>
        </div>
    </div>
</div>

<div id="append-detail" class="tbl-row tbl-row-data" style="display: none;">
    {foreach $attrItemsColumns as $attr}
    <div class="tbl-col">
        {$attr}
    </div>
    {/foreach}
    <div class="tbl-col tbl-col-icon">
        <div class="glyphicon glyphicon-trash remove-row-action"></div>
    </div>
</div>

<div id="append-sum" class="tbl-row tbl-header tbl-row-sum" style="display: none;">
    <div class="tbl-col">

    </div>
    <div class="tbl-col">

    </div>
    <div class="tbl-col">
        Итого
    </div>
    <div class="tbl-col">
        <span class="painter-sum"></span>
    </div>
    <div class="tbl-col">
        <span class="prepare-sum"></span>
    </div>
    <div class="tbl-col tbl-col-icon">

    </div>
</div>

<div id="append-painter-sum" class="tbl-row tbl-row-painter-sum" style="display: none;">
    <div class="tbl-col">

    </div>
    <div class="tbl-col">

    </div>
    <div class="tbl-col">
        <span class="painter-name"></span>
    </div>
    <div class="tbl-col">
        <span class="painter-sum"></span>
    </div>
    <div class="tbl-col">
    </div>
    <div class="tbl-col tbl-col-icon">

    </div>
</div>

<script src="/public/js/jquery-2.1.4.min.js"></script>
<script src="/public/js/jquery.serializejson.min.js"></script>
<script src="/public/js/jquery.popupoverlay.js"></script>
<script src="/public/js/jquery.datetimepicker.js"></script>
<script src="/public/js/main.js"></script>
<script src="/public/js/ordereditor.js"></script>