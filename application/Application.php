<?php

class Application {

    private $db;
    private $router;
    private $templater;
    private $request;
    private $user;
    private $attrParser;
    private $util;

    public function __construct() {
        $this->registerClasses();
        $this->util = new \Core\Util($this);
        $this->db = new \Core\DB($this);
        $this->router = new \Core\Router($this);
        $this->router->loadFromConfig(\CONF::$ROUTES);
        $this->templater = new \Core\Templater();
        $this->request = new \Core\Request($this);
        $this->attrParser = new \Core\AttrParser($this);
        

        $user = $this->db->createObject('\Models\User');
        $this->user = $user->getFromSession();

        \Core\Lang::loadLang('Ru');
    }

    public function getDb() {
        return $this->db;
    }

    public function getRouter() {
        return $this->router;
    }

    public function getTemplater() {
        return $this->templater;
    }

    public function getRequest() {
        return $this->request;
    }

    public function getUser() {
        return $this->user;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    function getAttrParser() {
        return $this->attrParser;
    }

    function getUtil() {
        return $this->util;
    }

    public function run() {
        $this->router->dispatch();
    }

    private function registerClasses() {
        spl_autoload_register(function($className) {
            $className = ltrim($className, '\\');
            $fileName = '';
            $namespace = '';
            if ($lastNsPos = strrpos($className, '\\')) {
                $namespace = substr($className, 0, $lastNsPos);
                $className = substr($className, $lastNsPos + 1);
                $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;

            }
            $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
            
            if (!file_exists(\CONF::APP_DIR . $fileName)) {
                return false;
            }

            require_once \CONF::APP_DIR . $fileName;
        });
    }

}
