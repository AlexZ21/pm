<?php

namespace Models;

class OrderItem extends \Core\DBModel {

    static public $table = "order_items";
    static public $types = [
        'id' => 'INT',
        'order_id' => 'INT',
        'color_id' => 'INT',
        'detail_id' => 'INT',
        'count' => 'INT',
        'painter_id' => 'INT',
        'painter_price' => 'INT',
        'prepare_price' => 'INT'
    ];
    static public $typesInfo = [
        'id' => ['type' => 'INT', 'editable' => FALSE],
        'order_id' => ['type' => 'INT', 'editable' => TRUE],
        'color_id' => ['type' => 'DROPDOWN', 'table' => 'colors',
            'editable' => TRUE],
        'detail_id' => ['type' => 'DROPDOWN', 'table' => 'details', 'fields' => ['paint_price', 'prepare_price'],
            'editable' => TRUE],
        'count' => ['type' => 'INT', 'editable' => TRUE],
        'painter_id' => ['type' => 'DROPDOWN', 'table' => 'painters',
            'editable' => TRUE],
        'painter_price' => ['type' => 'INT', 'editable' => FALSE],
        'prepare_price' => ['type' => 'INT', 'editable' => FALSE],
    ];
    protected $id;
    protected $order_id;
    protected $color_id;
    protected $detail_id;
    protected $count;
    protected $painter_id;
    protected $painter_price;
    protected $prepare_price;

    function __construct($app, $new = TRUE) {
        parent::__construct($app, $new);
    }

    function getId() {
        return $this->id;
    }

    function getOrderId() {
        return $this->order_id;
    }

    function getDetailId() {
        return $this->detail_id;
    }

    function getColorId() {
        return $this->color_id;
    }

    function getCount() {
        return $this->count;
    }

    function getPainterId() {
        return $this->painter_id;
    }

    function getPainterPrice() {
        return $this->painter_price;
    }

    function getPreparePrice() {
        return $this->prepare_price;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setOrderId($order_id) {
        $this->order_id = $order_id;
    }

    function setColorId($color_id) {
        $this->color_id = $color_id;
    }

    function setDetailId($detail_id) {
        $this->detail_id = $detail_id;
    }

    function setCount($count) {
        $this->count = $count;
    }

    function setPainterId($painter_id) {
        $this->painter_id = $painter_id;
    }

    function setPainterPrice($painter_price) {
        $this->painter_price = $painter_price;
    }

    function setPreparePrice($prepare_price) {
        $this->prepare_price = $prepare_price;
    }

}
