<?php

namespace Models;

class Detail extends \Core\DBModel {

    static public $table = "details";
    static public $types = [
        'id' => 'INT',
        'name' => 'STR',
        'prepare_price' => 'INT',
        'paint_price' => 'INT'
    ];
    static public $typesInfo = [
        'id' => ['type' => 'INT',
            'editable' => FALSE],
        'name' => ['type' => 'STR',
            'editable' => TRUE],
        'prepare_price' => ['type' => 'INT',
            'editable' => TRUE],
        'paint_price' => ['type' => 'INT',
            'editable' => TRUE]
    ];
    protected $id;
    protected $name = "";
    protected $prepare_price = 0;
    protected $paint_price = 0;

    function __construct($app, $new = TRUE) {
        parent::__construct($app, $new);
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getPreparePrice() {
        return $this->prepare_price;
    }

    function getPaintPrice() {
        return $this->paint_price;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setPreparePrice($prepare_price) {
        $this->prepare_price = $prepare_price;
    }

    function setPaintPrice($paint_price) {
        $this->paint_price = $paint_price;
    }

}
