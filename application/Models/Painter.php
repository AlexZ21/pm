<?php

namespace Models;

class Painter extends \Core\DBModel {

    static public $table = "painters";
    static public $types = [
        'id' => 'INT',
        'name' => 'STR'
    ];
        static public $typesInfo = [
        'id' => ['type' => 'INT',
            'editable' => FALSE],
        'name' => ['type' => 'STR',
            'editable' => TRUE]
    ];
    protected $id;
    protected $name = "";

    function __construct($app, $new = TRUE) {
        parent::__construct($app, $new);
    }

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

}
