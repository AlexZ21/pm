<?php

namespace Models;

class User extends \Core\DBModel {

    static public $table = "users";
    static public $types = [
        'id' => 'INT',
        'login' => 'STR',
        'password' => 'STR'
    ];
    protected $id;
    protected $login = "";
    protected $password = "";

    function __construct($app, $new = TRUE) {
        parent::__construct($app, $new);
    }

    function getId() {
        return $this->id;
    }

    function getLogin() {
        return $this->login;
    }

    function getPassword() {
        return $this->password;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    public function login() {
        if (session_status() != PHP_SESSION_ACTIVE) {
            session_start();
        }
        $_SESSION['id'] = $this->id;
    }

    public function logout() {
        if (session_status() == PHP_SESSION_ACTIVE) {
            session_destroy();
        }
    }

    public function isLogin() {
        
    }

    public function getFromSession() {
        if (session_status() != PHP_SESSION_ACTIVE) {
            session_start();
        }
        if (isset($_SESSION['id'])) {
            return $this->app->getDb()->getObject('\Models\User', ['id' => $_SESSION['id']]);
        }
    }

}
