<?php

namespace Models;

class Order extends \Core\DBModel {

    static public $table = "orders";
    static public $types = [
        'id' => 'INT',
        'status' => 'INT',
        'date' => 'DATETIME'
    ];
    static public $typesInfo = [
        'id' => ['type' => 'INT',
            'editable' => FALSE],
        'status' => ['type' => 'DROPDOWN', 'table' => 'order_status',
            'editable' => TRUE],
        'date' => ['type' => 'STR',
            'editable' => FALSE]
    ];
    protected $id;
    protected $status;
    protected $date;
    protected $orderItems = [];

    function __construct($app, $new = TRUE) {
        parent::__construct($app, $new);
    }

    function save() {
        parent::save();
        if (!$this->id) {
            $this->id = $this->app->getDb()->lastInsertedId('id');
        }
        foreach ($this->orderItems as $orderItem) {
            $orderItem->setOrderId($this->id);
            $orderItem->save();
        }
    }

    function delete() {
        foreach ($this->orderItems as $orderItem) {
            $orderItem->delete();
        }
        parent::delete();
    }

    function loadOrderItems() {
        if ($this->id) {
            $this->orderItems = $this->app->getDb()->getObject('\Models\OrderItem', ['order_id' => $this->id]);
            if (!$this->orderItems) {
                $this->orderItems = [];
            } else if (is_object($this->orderItems)) {
                $arr = [$this->orderItems];
                $this->orderItems = $arr;
            }
        } else {
            $this->orderItems = [];
        }
    }

    function addOrderItem($orderItem) {
        $this->orderItems[] = $orderItem;
    }

    function removeOrderItem($id) {
        foreach ($this->orderItems as $key => $orderItem) {
            if ($orderItem->getId() == $id) {
                $orderItem->delete();
                unset($this->orderItems[$key]);
            }
        }
    }

    function getOrderItems() {
        return $this->orderItems;
    }

    function getOrderItem($id) {
        foreach ($this->orderItems as $key => $orderItem) {
            if ($orderItem->getId() == $id) {
                return $this->orderItems[$key];
            }
        }
    }

    function getId() {
        return $this->id;
    }

    function getStatus() {
        return $this->status;
    }

    function getDate() {
        return $this->date;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setDate($date) {
        $this->date = $date;
    }

}
