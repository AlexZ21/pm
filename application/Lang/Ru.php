<?php

namespace Lang;

class Ru {

    static public $strings = [
        'lang_site_name' => 'PaintManager',
        
        // Painters list page
        'lang_painters_list_page_name' => 'Маляры',
        'lang_painters_list_table_header' => ['id' => '#', 'name' => 'Имя'],
        
        // Painters edit page
        'lang_painters_edit_page_name' => 'Редактировать запись',
        'lang_painters_create_page_name' => 'Создать запись',
        
        // Colors list page
        'lang_colors_list_page_name' => 'Цвета',
        'lang_colors_list_table_header' => ['id' => '#', 'name' => 'Наименование'],
        
        // Colors edit page
        'lang_colors_edit_page_name' => 'Редактировать цвет',
        'lang_colors_create_page_name' => 'Новый цвет',
        
        // Details list page
        'lang_details_list_page_name' => 'Детали',  
        'lang_details_list_table_header' => ['id' => '#', 'name' => 'Наименование', 
            'prepare_price' => 'Стоимость подготовки', 'paint_price' => 'Стоимость покраски'],
        
        // Details edit page
        'lang_details_edit_page_name' => 'Редактировать деталь',
        'lang_details_create_page_name' => 'Новая деталь',
        
        // Orders list page
        'lang_orders_list_page_name' => 'Заказы',
        'lang_orders_list_table_header' => ['id' => '#', 'status' => 'Статус', 'date' => 'Дата'],
        
        // Orders edit page
        'lang_orders_edit_page_name' => 'Редактировать заказ',
        'lang_orders_create_page_name' => 'Новый заказ',
        
        // Orders status
        'lang_order_status_process' => 'В работе',
        'lang_order_status_complite' => 'Завершен',     
        
        // Orders items list page
        'lang_orders_items_list_table_header' => ['id' => '#', 'order_id' => 'ID заказа', 
            'color_id' => 'Цвет', 'detail_id' => 'Деталь', 'count' => 'Количество', 
            'painter_id' => 'Маляр', 'painter_price' => 'Цена покраски', 'prepare_price' => 'Цена подготовки'],
        'lang_orders_items_create_page_name' => 'Новая деталь',
        'lang_orders_items_create_page_name' => 'Редактировать деталь',
    ];
    
}
