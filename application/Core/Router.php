<?php

namespace Core;

class Router {

    private $app;
    private $routes = ['GET' => [], 'POST' => [], 'PUT' => [], 'DELETE' => [], 'ALL' => []];
    private $patterns = [
        ':any' => '[^/]+',
        ':num' => '[0-9]+',
        ':all' => '.*'
    ];

    function __construct($app) {
        $this->app = $app;
    }

    public function get($route, $callback) {
        $this->routes['GET'][$route] = $callback;
    }

    public function post($route, $callback) {
        $this->routes['POST'][$route] = $callback;
    }

    public function put($route, $callback) {
        $this->routes['PUT'][$route] = $callback;
    }

    public function delete($route, $callback) {
        $this->routes['DELETE'][$route] = $callback;
    }

    public function dispatch() {
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $method = $_SERVER['REQUEST_METHOD'];

        $searches = array_keys($this->patterns);
        $replaces = array_values($this->patterns);

        if (array_key_exists($uri, $this->routes[$method])) {
            $this->execute($this->routes[$method][$uri]);
        } else if (array_key_exists($uri, $this->routes['ALL'])) {
            $this->execute($this->routes['ALL'][$uri]);
        } else {
            $found = FALSE;
            foreach ($this->routes[$method] as $route => $callback) {

                $replacedRoute = '';
                if (strpos($route, ':') !== false) {
                    $replacedRoute = str_replace($searches, $replaces, $route);
                }

                if (preg_match('#^' . $replacedRoute . '$#', $uri, $matched)) {
                    $this->execute($callback, $matched);
                    $found = TRUE;
                    break;
                }
            }

            if (!$found) {
                foreach ($this->routes['ALL'] as $route => $callback) {

                    $replacedRoute = '';
                    if (strpos($route, ':') !== false) {
                        $replacedRoute = str_replace($searches, $replaces, $route);
                    }

                    if (preg_match('#^' . $replacedRoute . '$#', $uri, $matched)) {
                    $this->execute($callback, $matched);                        
                        $found = TRUE;
                        break;
                    }
                }
            }
        }
    }

    public function redirect($uri) {
        $method = 'GET';
        if (array_key_exists($uri, $this->routes[$method])) {
            header('Location: ' . $uri, true, 303);
            $this->execute($this->routes[$method][$uri]);
        } else if (array_key_exists($uri, $this->routes['ALL'])) {
            header('Location: ' . $uri, true, 303);
            $this->execute($this->routes['ALL'][$uri]);
        }
    }

    public function execute($callback, $params = []) {
        if (!is_object($callback)) {
            $parts = explode('::', $callback);
            $controller = new $parts[0]($this->app);
            $controller->$parts[1]($params);
        } else {
            call_user_func($callback, $params);
        }
    }

    public function loadFromConfig($routes) {
        foreach ($routes as $route) {
            $this->routes[$route[1]][$route[0]] = $route[2];
        }
    }

}
