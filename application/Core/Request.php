<?php

namespace Core;

class Request {

    private $app;
    private $params = [];
    private $requestBody = '';

    function __construct($app) {
        $this->app = $app;
        $method = $_SERVER['REQUEST_METHOD'];
        $phpInput = file_get_contents('php://input');
        if ($method == "PUT" || $method == "DELETE") {
            if (!$this->app->getUtil()->isJson($phpInput)) {
                parse_str($phpInput, $this->params);
            }
        } else if ($method == "GET") {
            $this->params = $_GET;
        } else if ($method == "POST") {
            $this->params = $_POST;
        }

        $this->requestBody = $phpInput;
    }

    public function get($name, $default = null) {
        if (isset($this->params[$name])) {
            return $this->params[$name];
        } else {
            return $default;
        }
    }

    public function getCookie($name) {
        return $_COOKIE[$name];
    }

    public function getIP() {
        return $_SERVER['REMOTE_ADDR'];
    }

    function getRequestBody() {
        return $this->requestBody;
    }

    function parseRequestBodyFromJson() {
        return json_decode($this->requestBody, TRUE);
    }

}
