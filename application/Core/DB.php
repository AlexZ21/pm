<?php

namespace Core;

class DB {

    private $dbh;
    private $app;
    static public $types = [
        'INT' => [\PDO::PARAM_INT, ' = '],
        'STR' => [\PDO::PARAM_STR, ' LIKE '],
        'BOOL' => [\PDO::PARAM_BOOL, ' = '],
        'DATE' => [\PDO::PARAM_STR, ' LIKE '],
        'TIME' => [\PDO::PARAM_STR, ' LIKE '],
        'DATETIME' => [\PDO::PARAM_STR, ' LIKE '],
        'TIMESTAMP' => [\PDO::PARAM_INT, ' = ']
    ];

    public function __construct($app) {
        $this->app = $app;
        try {
            $this->dbh = new \PDO(\CONF::PDO_DSN, \CONF::PDO_USER, \CONF::PDO_PASS, [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
            $this->dbh->setAttribute(\PDO::ATTR_ERRMODE, \CONF::PDO_ATTR_ERRMODE);
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function getObject($className, $where = NULL, $order = NULL, $limit = NULL) {
        try {
            $sql = 'SELECT * FROM ' . $className::$table;
            $sqlBinds = [];

            if ($where) {
                $sql .= " WHERE ";
                $sp = ' AND ';
                $whereCount = count($where);
                $i = 0;
                foreach ($where as $key => $value) {
                    $sql .= $key . ' ' . $this::$types[$className::$types[$key]][1] . ' :' . $key;
                    $sqlBinds[':' . $key] = [$value, $className::$types[$key]];
                    $i++;
                    if ($i < $whereCount) {
                        $sql .= $sp;
                    }
                }
            }

            if ($order) {
                $sql .= " ORDER BY ";
                $sp = ', ';
                $orderCount = count($order);
                $i = 0;
                foreach ($order as $key => $value) {
                    $sql .= $key . ' ' . $value;
                    $i++;
                    if ($i < $orderCount) {
                        $sql .= $sp;
                    }
                }
            }

            if ($limit) {
                $sql .= " LIMIT " . $limit;
            }

            $result = $this->executePrepared($sql, $sqlBinds, $className);
            
            if (count($result) == 0) {
                return NULL;
            } else if (count($result) == 1) {
                return $result[0];
            }

            return $result;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function createObject($className) {
        return new $className($this->app);
    }

    public function saveObject($object) {
        if (!$object->isNew()) {
            $sql = "UPDATE " . $object::$table . ' SET ';
            $sqlBinds = [];

            $sp = ', ';
            $typesCount = count($object::$types);
            $i = 0;
            foreach ($object::$types as $key => $value) {

                $sql .= $key . ' = :' . $key;

                $sqlBinds[':' . $key] = [$object->get($key), $value];

                $i++;
                if ($i < $typesCount) {
                    $sql .= $sp;
                }
            }

            $sql .= ' WHERE id = :id';

            $this->executePrepared($sql, $sqlBinds);
        } else {
            $sql = 'INSERT INTO ' . $object::$table . ' (';
            $sqlValues = ') VALUES (';
            $sqlBinds = [];

            $sp = ', ';
            $typesCount = count($object::$types);
            $i = 0;
            foreach ($object::$types as $key => $value) {
                if ($key != 'id') {
                    $sql .= $key;
                    $sqlValues .= ':' . $key;
                    
                    $sqlBinds[':' . $key] = [$object->get($key), $value];

                    $i++;
                    if ($i < $typesCount-1) {
                        $sql .= $sp;
                        $sqlValues .= $sp;
                    }
                }
            }
            
            $sql .= $sqlValues.')';
            
            $this->executePrepared($sql, $sqlBinds);
        }
    }

    public function deleteObject($object) {
        if (!$object->isNew()) {
            $sql = "DELETE FROM " . $object::$table;
            $sqlBinds = [];

            $sqlBinds[':id'] = [$object->getId(), $object::$types['id']];
            $sql .= ' WHERE id = :id';

            $this->executePrepared($sql, $sqlBinds);
        }
    }

    public function executePrepared($sql, $binds, $className = NULL) {
        $sth = $this->dbh->prepare($sql);
        foreach ($binds as $key => $value) {
            $sth->bindParam($key, $value[0], $this::$types[$value[1]][0]);
        }
        if ($className) {
            $sth->setFetchMode(\PDO::FETCH_CLASS, $className, [$this->app, FALSE]);
        }
        $sth->execute();

        if ($sth->columnCount() > 0) {
            return $sth->fetchAll();
        } else {
            
        }
    }

    public function execute($sql) {
        return $this->dbh->exec($sql);
    }

    public function query($sql) {
        return $this->dbh->query($sql);
    }
    
    public function lastInsertedId($idColumn) {
        return $this->dbh->lastInsertId($idColumn);
    }

}
