<?php

namespace Core;

class Util {

    private $app;

    function __construct($app) {
        $this->app = $app;
    }

    function isJson($string, $returnData = FALSE) {
        $data = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE) ? ($returnData ? $data : TRUE) : FALSE;
    }

}
