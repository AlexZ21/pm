<?php

namespace Core;

class DBModel {

    protected $app;
    protected $new = TRUE;
    static public $table = "";
    static public $types = [];
    static public $editable = [];

    function __construct($app, $new = TRUE) {
        $this->app = $app;
        $this->new = $new;
    }

    public function save() {
        $this->app->getDb()->saveObject($this);
    }

    public function delete() {
        $this->app->getDb()->deleteObject($this);
    }

    public function toArray() {
        $arr = [];
        foreach (static::$types as $key => $value) {
            $arr[$key] = $this->$key;
        }
        return $arr;
    }
    
    public function isNew() {
        return $this->new;
    }

    public function get($name) {
        if (isset(static::$types[$name])) {
            return $this->$name;
        } else {
            return NULL;
        }
    }

    public function set($name, $value) {
        if (isset(static::$types[$name])) {
            $this->$name = $value;
        }
    }

}
