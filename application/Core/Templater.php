<?php

namespace Core;

require_once \CONF::FENOM_DIR.'/Fenom.php';

class Templater {

    private $fenom;

    function __construct() {
        \Fenom::registerAutoload(\CONF::FENOM_DIR);
        $this->fenom = \Fenom::factory(\CONF::VIEW_DIR, \CONF::CACHE_VIEW_DIR, 
                \Fenom::AUTO_RELOAD | \Fenom::FORCE_VERIFY);
    }

    public function getFenom() {
        return $this->fenom;
    }
    
    public function render($tpl, $attr = []) {
        $this->fenom->display($tpl, $attr);
    }
    
    public function fetch($tpl, $attr = []) {
        return $this->fenom->fetch($tpl, $attr);
    }    

}
