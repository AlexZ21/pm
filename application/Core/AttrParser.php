<?php

namespace Core;

class AttrParser {

    private $t;
    private $app;
    private $elements = [
        'INT' => 'elements/int.tpl',
        'STR' => 'elements/str.tpl',
        'DATETIME' => 'elements/datetime.tpl',
        'DROPDOWN' => 'elements/dropdown.tpl'
    ];
    private $editableElements = [
        'INT' => 'elements/editable/int.tpl',
        'STR' => 'elements/editable/str.tpl',
        'DATETIME' => 'elements/editable/datetime.tpl',
        'DROPDOWN' => 'elements/editable/dropdown.tpl'
    ];
    private $data = [];

    function __construct($app) {
        $this->app = $app;
        $this->t = $app->getTemplater();
        $this->db = $app->getDb();
    }

    public function parseAttr($object, $attrName, $editable = NULL) {

        if (is_null($editable)) {
            $editable = $object::$typesInfo[$attrName]['editable'];
        }

        if ($editable) {

            $method = 'parseEditable' . $object::$typesInfo[$attrName]['type'];
            return $this->$method($object->get($attrName), $attrName, $object::$typesInfo[$attrName]);
        } else {

            $method = 'parse' . $object::$typesInfo[$attrName]['type'];
            return $this->$method($object->get($attrName), $attrName, $object::$typesInfo[$attrName]);
        }
    }

    public function parseEmptyAttr($className, $attrName, $editable = NULL) {

        if (is_null($editable)) {
            $editable = $className::$typesInfo[$attrName]['editable'];
        }

        if ($editable) {

            $method = 'parseEditable' . $className::$typesInfo[$attrName]['type'];
            return $this->$method(NULL, $attrName, $className::$typesInfo[$attrName]);
        } else {

            $method = 'parse' . $className::$typesInfo[$attrName]['type'];
            return $this->$method(NULL, $attrName, $className::$typesInfo[$attrName]);
        }
    }

    public function parseValueAttr($className, $attrName, $value, $editable = NULL) {

        if (is_null($editable)) {
            $editable = $className::$typesInfo[$attrName]['editable'];
        }

        if ($editable) {

            $method = 'parseEditable' . $className::$typesInfo[$attrName]['type'];
            return $this->$method($value, $attrName, $className::$typesInfo[$attrName]);
        } else {

            $method = 'parse' . $className::$typesInfo[$attrName]['type'];
            return $this->$method($value, $attrName, $className::$typesInfo[$attrName]);
        }
    }

    public function parseEditableINT($value, $attrName, $typeInfo) {
        return $this->t->fetch($this->editableElements['INT'], ['name' => $attrName, 'value' => $value]);
    }

    public function parseINT($value, $attrName, $typeInfo) {
        return $this->t->fetch($this->elements['INT'], ['name' => $attrName, 'value' => $value]);
    }

    public function parseEditableSTR($value, $attrName, $typeInfo) {
        return $this->t->fetch($this->editableElements['STR'], ['name' => $attrName, 'value' => $value]);
    }

    public function parseSTR($value, $attrName, $typeInfo) {
        return $this->t->fetch($this->elements['STR'], ['name' => $attrName, 'value' => $value]);
    }

    public function parseEditableDATETIME($value, $attrName, $typeInfo) {
        return $this->t->fetch($this->editableElements['DATETIME'], ['name' => $attrName, 'value' => $value]);
    }

    public function parseDATETIME($value, $attrName, $typeInfo) {
        return $this->t->fetch($this->elements['DATETIME'], ['name' => $attrName, 'value' => $value]);
    }

    public function parseEditableDROPDOWN($value, $attrName, $typeInfo) {

        if (!isset($this->data[$typeInfo['table']])) {
            $db = $this->app->getDb();
            $result = $db->query('SELECT * FROM ' . $typeInfo['table'] . ' ORDER BY name');

            while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                if (\Core\Lang::get('lang_' . $typeInfo['table'] . '_' . $row['name'])) {
                    $row['title'] = \Core\Lang::get('lang_' . $typeInfo['table'] . '_' . $row['name']);
                } else {
                    $row['title'] = $row['name'];
                }

                if (isset($typeInfo['fields'])) {
                    $row['attrFields'] = $typeInfo['fields'];
                }
                $this->data[$typeInfo['table']][$row['id']] = $row;
            }
        }

        $options = $this->data[$typeInfo['table']];
        if ($value) {
            $options[$value]['selected'] = "selected";
        }

        return $this->t->fetch($this->editableElements['DROPDOWN'], ['name' => $attrName, 'options' => $options]);
    }

    public function parseDROPDOWN($value, $attrName, $typeInfo) {
        if (!isset($this->data[$typeInfo['table']])) {
            $db = $this->app->getDb();
            $result = $db->query('SELECT * FROM ' . $typeInfo['table'] . ' ORDER BY name');

            while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {
                if (\Core\Lang::get('lang_' . $typeInfo['table'] . '_' . $row['name'])) {
                    $row['title'] = \Core\Lang::get('lang_' . $typeInfo['table'] . '_' . $row['name']);
                } else {
                    $row['title'] = $row['name'];
                }
                if (isset($typeInfo['fields'])) {
                    $row['attrFields'] = $typeInfo['fields'];
                }
                $this->data[$typeInfo['table']][$row['id']] = $row;
            }
        }

        $option = [];

        if (!$value) {
            if (isset($typeInfo['fields'])) {
                $option['attrFields'] = $typeInfo['fields'];
                foreach ($option['attrFields'] as $field) {
                    $option[$field] = '';
                }
            }
        } else {
            $option = $this->data[$typeInfo['table']][$value];
        }

        return $this->t->fetch($this->elements['DROPDOWN'], ['name' => $attrName,
                    'option' => $option]);
    }

}
