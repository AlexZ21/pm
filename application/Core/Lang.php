<?php

namespace Core;

class Lang {

    static private $strings = [
    ];
    
    static public function loadLang($lang) {
        $langClass = '\Lang\\'.$lang;
        static::$strings = $langClass::$strings;
    }

    static public function get($name) {
        if (isset(static::$strings[$name])) {
            return static::$strings[$name];
        } else {
            return '';
        }
    }
    
    static public function toArray() {
        return static::$strings;
    }

}
