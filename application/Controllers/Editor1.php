<?php

namespace Controllers;

class Editor1 extends \Core\Controller {

    private $t;
    private $db;
    private $req;
    private $models = [
        'painters' => '\Models\Painter',
        'colors' => '\Models\Color',
        'details' => '\Models\Detail',
        'orders' => '\Models\Order'
    ];

    function __construct($app) {
        parent::__construct($app);
        $this->t = $app->getTemplater();
        $this->db = $app->getDb();
        $this->rec = $app->getRequest();
    }

    public function drawListPage($matches) {
        if ($this->app->getUser()) {
            $objectUri = $matches[1];

            $params = [];

            $params['pagetitle'] = \Core\Lang::get('lang_' . $objectUri . '_list_page_name');
            $params['tableHeader'] = \Core\Lang::get('lang_' . $objectUri . '_list_table_header');
            $params['data'] = [];
            $params['createLink'] = '/create/' . $objectUri;
            $params['removeLink'] = '/list/' . $objectUri;

            $ap = $this->app->getAttrParser();

            $objects = $this->db->getObject($this->models[$objectUri]);

            if (is_array($objects)) {
                foreach ($objects as $object) {

                    $values = [];

                    foreach ($object::$typesInfo as $key => $value) {
                        $values[] = $ap->parseAttr($object, $key, FALSE);
                    }

                    $params['data'][] = ['object' => $values,
                        'editUrl' => '/edit/' . $objectUri . '?id=' . $object->getId(),
                        'id' => $object->getId()];
                }
            } else if (is_object($objects)) {

                $values = [];

                foreach ($objects::$typesInfo as $key => $value) {
                    $values[] = $ap->parseAttr($objects, $key, FALSE);
                }

                $params['data'][] = ['object' => $values,
                    'editUrl' => '/edit/' . $objectUri . '?id=' . $objects->getId(),
                    'id' => $objects->getId()];
            }

            $content = $this->t->fetch('list1.tpl', $params);
            $this->t->render('base.tpl', array_merge(\Core\Lang::toArray(), ['content' => $content]));
        } else {
            $this->app->getRouter()->redirect('/login');
        }
    }

    public function drawEditPage($matches) {
        if ($this->app->getUser()) {
            $objectUri = $matches[1];

            $params = [];

            $objectId = $this->rec->get('id');

            $params['pagetitle'] = \Core\Lang::get('lang_' . $objectUri . '_edit_page_name');
            $params['action'] = '/edit/' . $objectUri;
            $params['listLink'] = '/list/' . $objectUri;

            $object = $this->db->getObject($this->models[$objectUri], ['id' => $objectId]);

            if ($object) {

                $ap = $this->app->getAttrParser();

                $attrNames = \Core\Lang::get('lang_' . $objectUri . '_list_table_header');

                foreach ($object::$typesInfo as $key => $value) {
                    $params['attrs'][] = [
                        'title' => $attrNames[$key],
                        'name' => $key,
                        'attr' => $ap->parseAttr($object, $key)
                    ];
                }

                $params['id'] = $object->getId();

                $content = $this->t->fetch('editor1.tpl', $params);
                $this->t->render('base.tpl', array_merge(\Core\Lang::toArray(), ['content' => $content]));
            } else {
                $this->app->getRouter()->redirect('/list/' . $objectUri);
            }
        } else {
            $this->app->getRouter()->redirect('/login');
        }
    }

    public function drawCreatePage($matches) {
        if ($this->app->getUser()) {
            $objectUri = $matches[1];

            $params = [];

            $params['pagetitle'] = \Core\Lang::get('lang_' . $objectUri . '_create_page_name');
            $params['action'] = '/edit/' . $objectUri;
            $params['listLink'] = '/list/' . $objectUri;

            $className = $this->models[$objectUri];

            $ap = $this->app->getAttrParser();

            $attrNames = \Core\Lang::get('lang_' . $objectUri . '_list_table_header');

            foreach ($className::$typesInfo as $key => $value) {
                $params['attrs'][] = [
                    'title' => $attrNames[$key],
                    'name' => $key,
                    'attr' => $ap->parseEmptyAttr($className, $key)
                ];
            }

            $content = $this->t->fetch('editor1.tpl', $params);
            $this->t->render('base.tpl', array_merge(\Core\Lang::toArray(), ['content' => $content]));
        } else {
            $this->app->getRouter()->redirect('/login');
        }
    }

    public function save($matches) {
        if ($this->app->getUser()) {
            $objectUri = $matches[1];
            
            $fields = $this->rec->parseRequestBodyFromJson();

            $objectId = $fields['id'];
            $object = $this->db->getObject($this->models[$objectUri], ['id' => $objectId]);

            if (!$object) {
                $object = $this->db->createObject($this->models[$objectUri]);
            }

            foreach ($object::$types as $key => $value) {
                $object->set($key, $fields[$key]);
            }

            $object->save();

            echo '1';
        } else {
            $this->app->getRouter()->redirect('/login');
        }
    }

    public function delete($matches) {
        if ($this->app->getUser()) {
            $objectUri = $matches[1];
            
            $fields = $this->rec->parseRequestBodyFromJson();

            $objectId = $fields['id'];
            $object = $this->db->getObject($this->models[$objectUri], ['id' => $objectId]);

            if ($object) {
                $object->delete();
            }

            echo '1';
        } else {
            $this->app->getRouter()->redirect('/login');
        }
    }

    public function deleteObjects($matches) {
        if ($this->app->getUser()) {
            $objectUri = $matches[1];
            $sql = 'DELETE FROM ' . $objectUri . ' WHERE id IN ';
            $ids = $this->rec->parseRequestBodyFromJson();
            
            $sqlPlaceholders = [];

            for ($i = 0; $i < count($ids); $i++) {
                $sqlPlaceholders[] = ':id' . $i;
                $sqlBinds[':id' . $i] = [$ids[$i], 'INT'];
            }

            $sql .= '(' . implode(',', $sqlPlaceholders) . ')';

            $this->db->executePrepared($sql, $sqlBinds);

            echo '1';
        } else {
            $this->app->getRouter()->redirect('/login');
        }
    }

}
