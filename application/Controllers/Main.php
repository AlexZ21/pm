<?php

namespace Controllers;

class Main extends \Core\Controller {

    private $t;
    private $db;
    private $req;

    function __construct($app) {
        parent::__construct($app);
        $this->t = $app->getTemplater();
        $this->db = $app->getDb();
        $this->rec = $app->getRequest();
    }

    public function drawMain() {

        if ($this->app->getUser()) {

            $this->app->getRouter()->redirect('/list/orders');
        } else {
            $this->app->getRouter()->redirect('/login');
        }
    }

}
