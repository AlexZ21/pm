<?php

namespace Controllers;

class User extends \Core\Controller {

    private $t;
    private $db;
    private $req;

    function __construct($app) {
        parent::__construct($app);
        $this->t = $app->getTemplater();
        $this->db = $app->getDb();
        $this->rec = $app->getRequest();
    }

    public function drawLogin() {

        $ip = $this->rec->getIP();
        $securityHash = sha1(time() . rand() . $ip);

        $this->db->execute('DELETE FROM loginhash WHERE deadline < NOW()');

        $this->db->executePrepared('INSERT INTO loginhash(ip, hash, deadline)
            VALUES(:ip, :hash, ADDTIME(NOW(), "0:10:0"))', [
            ':ip' => [$ip, 'STR'],
            ':hash' => [$securityHash, 'STR']
        ]);

        $this->t->render('login.tpl', ['securityHash' => $securityHash]);
    }

    public function login() {

        if ($this->rec->get('login')) {

            $login = $this->rec->get('login');
            $password = $this->rec->get('npassword');
            $ip = $this->rec->getIP();

            $user = $this->db->executePrepared('SELECT u.id AS id, u.login AS login, u.password AS password '
                    . 'FROM users AS u, loginhash AS lh '
                    . 'WHERE u.login = :login AND lh.ip = :ip AND SHA1(CONCAT(u.password, lh.hash)) = :password', [
                ':ip' => [$ip, 'STR'],
                ':login' => [$login, 'STR'],
                ':password' => [$password, 'STR']
                    ], '\Models\User');

            if (count($user) > 0) {
                $user = $user[0];
                $this->app->setUser($user);
                $user->login();
                $this->app->getRouter()->redirect('/');
            }
        }
    }
    
    public function logout() {
        if ($this->app->getUser()) {
            $this->app->getUser()->logout();
            $this->app->setUser(NULL);
        }
        $this->app->getRouter()->redirect('/');
    }

}
