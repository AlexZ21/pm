<?php

namespace Controllers;

class OrderEditor extends \Core\Controller {

    private $t;
    private $db;
    private $req;
    private $models = [
        'orders' => '\Models\Order'
    ];

    function __construct($app) {
        parent::__construct($app);
        $this->t = $app->getTemplater();
        $this->db = $app->getDb();
        $this->rec = $app->getRequest();
    }

    public function drawListPage($matches) {
        if ($this->app->getUser()) {

            $params = [];

            $params['pagetitle'] = \Core\Lang::get('lang_orders_list_page_name');
            $params['tableHeader'] = \Core\Lang::get('lang_orders_list_table_header');
            $params['data'] = [];
            $params['createLink'] = '/create/orders';
            $params['removeLink'] = '/list/orders';

            $ap = $this->app->getAttrParser();

            $objects = $this->db->getObject('\Models\Order');

            if (is_array($objects)) {
                foreach ($objects as $object) {

                    $values = [];

                    foreach ($object::$typesInfo as $key => $value) {
                        $values[] = $ap->parseAttr($object, $key, FALSE);
                    }

                    $params['data'][] = ['object' => $values,
                        'editUrl' => '/edit/orders?id=' . $object->getId(),
                        'id' => $object->getId()];
                }
            } else if (is_object($objects)) {

                $values = [];

                foreach ($objects::$typesInfo as $key => $value) {
                    $values[] = $ap->parseAttr($objects, $key, FALSE);
                }

                $params['data'][] = ['object' => $values,
                    'editUrl' => '/edit/orders?id=' . $objects->getId(),
                    'id' => $objects->getId()];
            }

            $content = $this->t->fetch('list1.tpl', $params);
            $this->t->render('base.tpl', array_merge(\Core\Lang::toArray(), ['content' => $content]));
        } else {
            $this->app->getRouter()->redirect('/login');
        }
    }

    public function drawEditPage($matches) {
        if ($this->app->getUser()) {
            $params = [];

            $objectId = $this->rec->get('id');

            $params['pagetitle'] = \Core\Lang::get('lang_orders_edit_page_name');
            $params['action'] = '/edit/orders';
            $params['listLink'] = '/list/orders';

            $object = $this->db->getObject('\Models\Order', ['id' => $objectId]);

            $object->loadOrderItems();

            if ($object) {

                $ap = $this->app->getAttrParser();

                $attrNames = \Core\Lang::get('lang_orders_list_table_header');

                foreach ($object::$typesInfo as $key => $value) {
                    $params['attrs'][] = [
                        'title' => $attrNames[$key],
                        'name' => $key,
                        'attr' => $ap->parseAttr($object, $key)
                    ];
                }

                $params['id'] = $object->getId();


                $orderItems = '\Models\OrderItem';
                $params['colors'] = $ap->parseEmptyAttr($orderItems, 'color_id');
                $attrItemsNames = \Core\Lang::get('lang_orders_items_list_table_header');

                foreach ($orderItems::$typesInfo as $key => $value) {
                    if ($key != 'id' && $key != 'order_id') {
                        $params['attrItems'][] = [
                            'title' => $attrItemsNames[$key],
                            'name' => $key,
                            'attr' => $ap->parseEmptyAttr($orderItems, $key)
                        ];
                    }
                }

                $params['orderItems'] = [];
                foreach ($object->getOrderItems() as $orderItem) {



                    $orderItemData = ['id' => $orderItem->getId(), 'attrs' => []];

                    foreach ($orderItem::$typesInfo as $key => $value) {
                        if ($key != 'id' && $key != 'color_id' && $key != 'order_id') {
                            $orderItemData['attrs'][] = $ap->parseAttr($orderItem, $key, FALSE);
                        }
                    }

                    $params['orderItems'][$orderItem->getColorId()]['orderItemColor'] = $ap->parseAttr($orderItem, 'color_id', FALSE);
                    $params['orderItems'][$orderItem->getColorId()]['orderItems'][] = $orderItemData;
                }

                foreach ($orderItems::$typesInfo as $key => $value) {
                    if ($key != 'id' && $key != 'color_id' && $key != 'order_id') {
                        $params['attrItemsColumns'][] = $ap->parseEmptyAttr($orderItems, $key, FALSE);
                    }
                }

                $content = $this->t->fetch('ordereditor.tpl', $params);
                $this->t->render('base.tpl', array_merge(\Core\Lang::toArray(), ['content' => $content]));
            } else {
                $this->app->getRouter()->redirect('/list/orders');
            }
        } else {
            $this->app->getRouter()->redirect('/login');
        }
    }

    public function drawCreatePage($matches) {
        if ($this->app->getUser()) {
            $params = [];

            $params['pagetitle'] = \Core\Lang::get('lang_orders_create_page_name');
            $params['action'] = '/edit/orders';
            $params['listLink'] = '/list/orders';

            $className = '\Models\Order';

            $ap = $this->app->getAttrParser();

            $attrNames = \Core\Lang::get('lang_orders_list_table_header');

            foreach ($className::$typesInfo as $key => $value) {
                $params['attrs'][] = [
                    'title' => $attrNames[$key],
                    'name' => $key,
                    'attr' => $ap->parseEmptyAttr($className, $key)
                ];
            }

            $orderItems = '\Models\OrderItem';
            $params['colors'] = $ap->parseEmptyAttr($orderItems, 'color_id');
            $attrItemsNames = \Core\Lang::get('lang_orders_items_list_table_header');

            foreach ($orderItems::$typesInfo as $key => $value) {
                if ($key != 'id' && $key != 'order_id') {
                    $params['attrItems'][] = [
                        'title' => $attrItemsNames[$key],
                        'name' => $key,
                        'attr' => $ap->parseEmptyAttr($orderItems, $key)
                    ];
                }
            }

            foreach ($orderItems::$typesInfo as $key => $value) {
                if ($key != 'id' && $key != 'color_id' && $key != 'order_id') {
                    $params['attrItemsColumns'][] = $ap->parseEmptyAttr($orderItems, $key, FALSE);
                }
            }

            $content = $this->t->fetch('ordereditor.tpl', $params);
            $this->t->render('base.tpl', array_merge(\Core\Lang::toArray(), ['content' => $content]));
        } else {
            $this->app->getRouter()->redirect('/login');
        }
    }

    public function save($matches) {
        if ($this->app->getUser()) {

            $fields = $this->rec->parseRequestBodyFromJson();
            
            $objectId = $fields['id'];
            $order = $this->db->getObject('\Models\Order', ['id' => $objectId]);

            if (!$order) {
                $order = $this->db->createObject('\Models\Order');
            }

            $order->loadOrderItems();

            foreach ($order::$types as $key => $value) {
                $order->set($key, $fields[$key]);
            }

            foreach ($fields['orderItems']['removedIds'] as $id) {
                $order->removeOrderItem($id);
            }

            foreach ($fields['orderItems']['new'] as $colorId => $orderItemFields) {

                foreach ($orderItemFields as $orderItemField) {
                    $orderItem = $this->db->createObject('\Models\OrderItem');
                    $orderItem->setColorId($colorId);
                    foreach ($orderItem::$types as $key => $value) {
                        if (isset($orderItemField[$key])) {
                            $orderItem->set($key, $orderItemField[$key]);
                        }
                    }
                    $order->addOrderItem($orderItem);
                }
            }



            foreach ($fields['orderItems']['ids'] as $colorId => $rowIds) {

                foreach ($rowIds as $rowId => $orderItemFields) {

                    $orderItem = $order->getOrderItem($rowId);
                    $orderItem->setColorId($colorId);
                    foreach ($orderItem::$types as $key => $value) {
                        if (isset($orderItemFields[$key])) {
                            $orderItem->set($key, $orderItemFields[$key]);
                        }
                    }
                }
            }


            $order->save();

            echo '1';
        } else {
            $this->app->getRouter()->redirect('/login');
        }
    }

    public function delete($matches) {
        if ($this->app->getUser()) {

            $fields = $this->rec->parseRequestBodyFromJson();

            $objectId = $fields['id'];
            $object = $this->db->getObject('\Models\Order', ['id' => $objectId]);

            if ($object) {
                $object->delete();
            }

            echo '1';
        } else {
            $this->app->getRouter()->redirect('/login');
        }
    }

    public function deleteObjects($matches) {
        if ($this->app->getUser()) {
            $sql = 'DELETE FROM orders WHERE id IN ';
            $ids = $this->rec->parseRequestBodyFromJson();

            $sqlPlaceholders = [];

            for ($i = 0; $i < count($ids); $i++) {
                $sqlPlaceholders[] = ':id' . $i;
                $sqlBinds[':id' . $i] = [$ids[$i], 'INT'];
            }

            $sql .= '(' . implode(',', $sqlPlaceholders) . ')';

            $this->db->executePrepared($sql, $sqlBinds);

            echo '1';
        } else {
            $this->app->getRouter()->redirect('/login');
        }
    }

}
