(function ($) {

    var methods = {
        init: function (options) {
            var self = this;
            $(this).popup({
                transition: 'all 0.3s',
                scrolllock: true
            });
            
            $('.btn-close', this).click(function(){
                $(self).popup('hide');
            });
        }
    };

    $.fn.pmPopup = function (methodOrOptions) {
        if (methods[methodOrOptions]) {
            return methods[ methodOrOptions ].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + methodOrOptions + ' does not exist on jQuery.tooltip');
        }
    };


})(jQuery);

$(function () {

    $('.content-wrapper').height($('.wrapper').height() - $('header').height());

    $('body').css({'overflow': 'auto'});

    $(window).resize(function () {
        contentWrapperResize();
    });

    $('.popup').pmPopup();

});

function contentWrapperResize() {
    $('.content-wrapper').height($('.wrapper').height() - $('header').height());
}