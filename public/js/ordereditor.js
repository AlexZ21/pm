var removedOrderItems = [];

$(function () {
//    $(':input[name=date]').datetimepicker({
//        format:'Y-m-d H:i'
//    });
//    $(':input[name=date]').parent().find('span')

    if(!$(':input[name=date]').val()) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        } 

        if(mm<10) {
            mm='0'+mm
        } 

        today = yyyy + '-' + mm + '-' + dd;

        $(':input[name=date]').parent().find('span').text(today);
        $(':input[name=date]').val(today);
    }

    updateOrderSum();

    //Editor 1
    $('#action-save').click(function (e) {
        e.preventDefault();

        var data = $('#editor1-form').serializeJSON();
        data.orderItems = {
            'ids': {},
            'new': {},
            'removedIds': removedOrderItems
        };

        $('.orderlist .color-group').each(function (i, el) {

            var colorId = $(el).attr('data-color-id');

            $('.tbl-row-data', el).each(function (i, rowEl) {

                var rowId = $(this).attr('data-id');
                if (typeof rowId !== typeof undefined && rowId !== false) {


                    if (!data.orderItems.ids.hasOwnProperty(colorId)) {
                        data.orderItems.ids[colorId] = {};
                    }

                    data.orderItems.ids[colorId][rowId] = {};
                    var $fields = $(':input', rowEl);

                    $fields.each(function (i, field) {
                        var attrName = $(field).attr('name');
                        data.orderItems.ids[colorId][rowId][attrName] = $(field).val();
                    });

                } else {

                    if (!data.orderItems.new.hasOwnProperty(colorId)) {
                        data.orderItems.new[colorId] = [];
                    }

                    var newItem = {};
                    var $fields = $(':input', rowEl);
                    $fields.each(function (i, field) {
                        var attrName = $(field).attr('name');
                        newItem[attrName] = $(field).val();
                    });
                    data.orderItems.new[colorId].push(newItem);
                }
            });

        });


        $.ajax({
            type: "POST",
            url: $('#editor1-form').attr('action'),
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                window.location.href = $('#action-cancel').attr('href');
            }
        });

    });

    $('#action-delete').click(function (e) {
        e.preventDefault();

        var formData = $('#editor1-form').serializeJSON();

        $.ajax({
            url: $('#editor1-form').attr('action'),
            type: 'DELETE',
            data: JSON.stringify(formData),
            success: function (result) {
                window.location.href = $('#action-cancel').attr('href');
            }
        });

    });

    $('#action-print').click(function (e) {
        e.preventDefault();

        var printWindow = window.open('', 'Печать');
        printWindow.document.write('<html><head>');
        printWindow.document.write('<link rel="stylesheet" href="/public/css/print.css" type="text/css" />');
        printWindow.document.write('</head><body >');
        var $orderItemsField = $('.order-items-field').clone();
        $('.tbl-col-icon', $orderItemsField).remove();
        printWindow.document.write('<div class="date">' + $(':input[name=date]').val() + '</div>');
        printWindow.document.write($orderItemsField.html());
        printWindow.document.write('</body></html>');

        printWindow.document.close();
        printWindow.focus();

        printWindow.onload = function () {
            printWindow.print();
        };


        //printWindow.close();
    });


    $(document).on('click', '.tbl-row-data', function (e) {
        e.preventDefault();
        var className = $(e.target).attr('class') || '';
        if (className.indexOf('remove-row-action') !== -1) {
            removeOrderItem($(this));
        } else {

            $('#create-order-item-popup :input[name=color_id]').val($(this).parents('.color-group').attr('data-color-id'));
            $(':input', this).each(function (i, el) {
                $('#create-order-item-popup :input[name=' + $(el).attr('name') + ']').val($(el).val());
                if ($(el).attr('type') == 'hidden') {
                    $('#create-order-item-popup :input[name=' + $(el).attr('name') + ']').parent().
                            find('span').html($(el).parent().find('span').text());
                }
            });
            $(this).attr('data-edited', 1);
            $('#create-order-item-popup').attr('data-row-id', $(this).attr('data-id'));

            $('#create-order-item-popup :input[name=detail_id]').change(function (e) {
                var count = $('#create-order-item-popup :input[name=count]').val();
                var $detail = $('#create-order-item-popup :input[name=detail_id]').find(":selected");
                var painterPrice = $detail.attr('paint_price') * count;
                var preparePrice = $detail.attr('prepare_price') * count;
                $('#create-order-item-popup :input[name=painter_price]').val(painterPrice);
                $('#create-order-item-popup :input[name=painter_price]').parent().find('span').html(painterPrice);
                $('#create-order-item-popup :input[name=prepare_price]').val(preparePrice);
                $('#create-order-item-popup :input[name=prepare_price]').parent().find('span').html(preparePrice);
            });

            $('#create-order-item-popup :input[name=count]').change(function (e) {
                var count = $(this).val() || 0;
                var $detail = $('#create-order-item-popup :input[name=detail_id]').find(":selected");
                var painterPrice = $detail.attr('paint_price') * count;
                var preparePrice = $detail.attr('prepare_price') * count;
                $('#create-order-item-popup :input[name=painter_price]').val(painterPrice);
                $('#create-order-item-popup :input[name=painter_price]').parent().find('span').html(painterPrice);
                $('#create-order-item-popup :input[name=prepare_price]').val(preparePrice);
                $('#create-order-item-popup :input[name=prepare_price]').parent().find('span').html(preparePrice);
            });

            $('#create-order-item-popup').popup('show');

        }

    });


    $('#action-add-detail').click(function (e) {
        e.preventDefault();
        $('#create-order-item-popup .form-field').each(function (i, el) {
            $(':input', el).val('')
        });
        $('#create-order-item-popup').attr('data-row-id', -1);
        $('#create-order-item-popup :input[name=painter_price]').parent().find('span').html('');
        $('#create-order-item-popup :input[name=prepare_price]').parent().find('span').html('');

        $('#create-order-item-popup :input[name=detail_id]').change(function (e) {
            var count = $('#create-order-item-popup :input[name=count]').val();
            var $detail = $('#create-order-item-popup :input[name=detail_id]').find(":selected");
            var painterPrice = $detail.attr('paint_price') * count;
            var preparePrice = $detail.attr('prepare_price') * count;
            $('#create-order-item-popup :input[name=painter_price]').val(painterPrice);
            $('#create-order-item-popup :input[name=painter_price]').parent().find('span').html(painterPrice);
            $('#create-order-item-popup :input[name=prepare_price]').val(preparePrice);
            $('#create-order-item-popup :input[name=prepare_price]').parent().find('span').html(preparePrice);
        });

        $('#create-order-item-popup :input[name=count]').change(function (e) {
            var count = $(this).val() || 0;
            var $detail = $('#create-order-item-popup :input[name=detail_id]').find(":selected");
            var painterPrice = $detail.attr('paint_price') * count;
            var preparePrice = $detail.attr('prepare_price') * count;
            $('#create-order-item-popup :input[name=painter_price]').val(painterPrice);
            $('#create-order-item-popup :input[name=painter_price]').parent().find('span').html(painterPrice);
            $('#create-order-item-popup :input[name=prepare_price]').val(preparePrice);
            $('#create-order-item-popup :input[name=prepare_price]').parent().find('span').html(preparePrice);
        });

        $('#create-order-item-popup').popup('show');
    });
});

function addOrderItem() {

    var rowId = $('#create-order-item-popup').attr('data-row-id');

    var $editedRow = $('.tbl-row-data[data-edited=1]');

    if ($editedRow.length > 0) {
        var colorId = $('#create-order-item-popup :input[name=color_id]').val();

        if (colorId) {

            if ($editedRow.parent('.color-group').attr('data-color-id') == colorId) {
                updateOrderItem($editedRow);
            } else {
                updateOrderItem($editedRow);
                var $colorGroup = getColorGroup(colorId);
                $colorGroup.append($editedRow);
                checkColorGroupAndRemove();
            }
        }
        $editedRow.removeAttr('data-edited');

    } else {
        var colorId = $('#create-order-item-popup :input[name=color_id]').val();
        if (colorId) {

            var $colorGroup = getColorGroup(colorId);
            var $newOrderItem = createOrderItem();
            $colorGroup.append($newOrderItem);
        }
    }

    updateOrderSum();

    $('#create-order-item-popup').popup('hide');
}

function getColorGroup(colorId) {
    var $colorGroup = $('.color-group[data-color-id=' + colorId + ']');

    if ($colorGroup.length == 0) {
        $colorGroup = $('#append-color').clone();
        $colorGroup.attr('id', '');
        $colorGroup.css({'display': 'block'});
        $('.orderlist').append($colorGroup);
        $colorGroup.attr('data-color-id', colorId);
        $('.color-id', $colorGroup).html($('#create-order-item-popup :input[name=color_id]').find(":selected").text());
    }

    return $colorGroup;
}

function createOrderItem(id) {
    var $newOrderItem = $('#append-detail').clone();
    $newOrderItem.attr('id', '');
    $newOrderItem.css({'display': 'flex'});

    $('#create-order-item-popup .form-field').each(function (i, el) {

        $newOrderItem.find(':input[name=' + $(':input', el).attr('name') + ']').val($(':input', el).val());

        var text = $(':input', el).find(":selected").text();
        if (!text) {
            text = $(':input', el).val();
        }

        $newOrderItem.find(':input[name=' + $(':input', el).attr('name') + ']').parent().find('span').
                html(text);

        if (id) {
            $newOrderItem.attr('data-id', id);
        }
    });

    return $newOrderItem;
}

function updateOrderItem($editedRow) {
    var $newOrderItem = $editedRow;

    $('#create-order-item-popup .form-field').each(function (i, el) {

        $newOrderItem.find(':input[name=' + $(':input', el).attr('name') + ']').val($(':input', el).val());

        var text = $(':input', el).find(":selected").text();
        if (!text) {
            text = $(':input', el).val();
        }

        $newOrderItem.find(':input[name=' + $(':input', el).attr('name') + ']').parent().find('span').
                html(text);

    });

}

function removeOrderItem($row) {
    var rowId = $row.attr('data-id');
    if (typeof rowId !== typeof undefined && rowId !== false) {
        removedOrderItems.push(rowId);
    }
    if ($row.parents('.color-group').find('.tbl-row-data').length == 1) {
        $row.parents('.color-group').remove();
    } else {
        $row.remove();
    }

    updateOrderSum();
}

function checkColorGroupAndRemove() {

    $('.color-group').each(function (i, el) {
        if ($(el).find('.tbl-row-data').length == 0) {
            $(el).remove();
        }
    });

}

function updateOrderSum() {

    var sum = {
        painterPriceSum: 0,
        preparePriceSum: 0,
        painters: {}
    };

    $('.orderlist .color-group').each(function (i, colorGroup) {
        $('.tbl-row-data', colorGroup).each(function (i, row) {
            var painterPrice = $(':input[name=painter_price]', row).val();
            var preparerPrice = $(':input[name=prepare_price]', row).val();
            var painterId = $(':input[name=painter_id]', row).val();
            var painter = $(':input[name=painter_id]', row).parent().find('span').text();

            sum.painterPriceSum += parseInt(painterPrice);
            sum.preparePriceSum += parseInt(preparerPrice);

            if (painterId) {
                if (!sum.painters.hasOwnProperty(painterId)) {
                    sum.painters[painterId] = {
                        name: $.trim(painter),
                        sum: 0
                    };
                }

                sum.painters[painterId].sum += parseInt(painterPrice);
            }
        });



    });

    $('.orderlist .tbl-row-sum').remove();
    $('.orderlist .tbl-row-painter-sum').remove();

    var $rowSum = $('#append-sum').clone();
    $rowSum.attr('id', '');
    $rowSum.css({'display': 'flex'});
    $('.painter-sum', $rowSum).html(sum.painterPriceSum);
    $('.prepare-sum', $rowSum).html(sum.preparePriceSum);
    $('.orderlist').append($rowSum);

    $.each(sum.painters, function (i, painter) {
        var $rowSum = $('#append-painter-sum').clone();
        $rowSum.attr('id', '');
        $rowSum.css({'display': 'flex'});
        $('.painter-name', $rowSum).html(painter.name);
        $('.painter-sum', $rowSum).html(painter.sum);
        $('.orderlist').append($rowSum);
    });

    console.log(sum);
}

function onSubmit() {
    return false;
}