$(function () {

    $('#list1-delete').click(function (e) {
        e.preventDefault();

        var checked = $('input[type=checkbox]:checked:enabled', '.list1');

        if (checked.length > 0) {

            var ids = [];

            checked.each(function (i, el) {
                ids.push($(el).parents('.tbl-row').attr('data-id'));
            });
            

            $.ajax({
                type: "DELETE",
                url: $('#list1-delete').attr('href'),
                data: JSON.stringify(ids),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    //window.location.href = $('#editor1-cancel').attr('href');

                    checked.each(function (i, el) {
                        $(el).parents('.tbl-row').remove();
                    });

                }
            });

        }
    });

});