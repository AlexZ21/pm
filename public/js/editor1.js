$(function () {

    //Editor 1
    $('#editor1-save').click(function (e) {
        e.preventDefault();

        var formData = $('#editor1-form').serializeJSON();
        

        $.ajax({
            type: "POST",
            url: $('#editor1-form').attr('action'),
            data: JSON.stringify(formData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                window.location.href = $('#editor1-cancel').attr('href');
            }
        });

    });

    $('#editor1-delete').click(function (e) {
        e.preventDefault();
        
        var formData = $('#editor1-form').serializeJSON();
        
        $.ajax({
            type: "DELETE",
            url: $('#editor1-form').attr('action'),
            data: JSON.stringify(formData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                window.location.href = $('#editor1-cancel').attr('href');
            }
        });        

    });

});

function onSubmit() {
    return false;
}